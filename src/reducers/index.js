import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

const initialState = {
    activeTab: 1,
    songList: [],
    songDetailList: []
}

//state = initialState means: mac dinh k co gi thi state = initialState
export const actionReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'tabClicked':
            return {
                ...state,
                activeTab: action.activeTab
            }
        case 'SHOW_SONG_LIST':
            return {
                ...state,
                songList: action.songID
            }
        case 'SHOW_SONG_DETAIL': {
            return {
                ...state,
                songDetailList: action.data
            }
        }
        default:
            return state;
            //Mac dinh return state da dc initialize
    }
};

//Create Store
export const reducers = combineReducers({ actionReducer });
export const store = createStore(reducers, applyMiddleware(thunk));