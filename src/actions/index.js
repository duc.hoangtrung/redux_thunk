

export const tabClicked = (activeTab) => ({
		type: 'tabClicked',
		activeTab: activeTab
});




export const getSongList = (songID) => ({
		type: 'SHOW_SONG_LIST',
    	songID: songID
});

