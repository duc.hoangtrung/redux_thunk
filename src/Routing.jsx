import { Redirect } from "react-router-dom";
import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import TabContainer from './components/tab-container/TabContainer';
//import songList from './components/songList/songList';

export class Routing extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" render={() => (
                    <Redirect to="/tabs" />
                )} />
                {/*<Route path="/song_db" component={songList} />*/}
                {/*songList Processing*/}
                <Route exact path='/tabs' component={TabContainer} />
            </Switch>
        )
    }
}


