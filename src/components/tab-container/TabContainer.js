import React, { Component } from 'react';

import TabComponent from '../tabs/TabComponent';
import DuplicateTabComponent from '../tabs/DuplicateTabComponent';

class TabContainer extends Component {

    render() {
        return (
            <React.Fragment>
                <DuplicateTabComponent />
                <TabComponent />
            </React.Fragment>
        );
    }
}

export default TabContainer;