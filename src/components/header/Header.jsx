import React, { Component } from 'react';
import './Header.css';
import { NavLink } from 'react-router-dom';

class Header extends Component {

    render() {
        return (
            <nav className="navBar">
                <NavLink className="navLink" to="/song_db" activeClassName="active" style={{textDecoration : 'none'}}>Song List</NavLink>
                <NavLink className="navLink" to="/tabs" activeClassName="active" style={{textDecoration : 'none'}}>Tabs</NavLink>
            </nav>
        );
    }
}

export default Header;

//exact={true}???