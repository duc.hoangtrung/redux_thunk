import React, { Component } from 'react';
import { connect } from 'react-redux';
import { tabClicked } from './../../actions';
import './TAB.css';

class TabComponent extends Component {

    render() {
        return (
            <React.Fragment>
                <nav className="navBar">
                    <a  
                    className = {"navTab" + (this.props.activeTab === 1 ? ' active' : '')} 
                    onClick = {() => this.props.tabClicked(1)}
                    href = "#nav-tab1"
                    style={{textDecoration : 'none'}}
                    >Tab 1</a>

                    <a  
                    className = {"navTab" + (this.props.activeTab === 2 ? ' active' : '')} 
                    onClick = {() => this.props.tabClicked(2)}
                    href = "#nav-tab2"
                    style={{textDecoration : 'none'}}
                    >Tab 2</a>

                    <a  
                    className = {"navTab" + (this.props.activeTab === 3 ? ' active' : '')} 
                    onClick = {() => this.props.tabClicked(3)}
                    href = "#nav-tab3"
                    style={{textDecoration : 'none'}}
                    >Tab 3</a>
                </nav>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        activeTab: state.actionReducer.activeTab,
    }
}

const mapDispatchToProps = { tabClicked };

const TabContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(TabComponent);
export default TabContainer;


