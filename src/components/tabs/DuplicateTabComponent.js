import React, { Component } from 'react';
import { connect } from 'react-redux';
import { tabClicked } from './../../actions';
import './TAB.css';

class DuplicateTabComponent extends Component {

    render() {
        return (
            <React.Fragment>
                <nav className="navBar">
                    <a  
                    className = {"navTab" + (this.props.activeTab === 1 ? ' active' : '')} 
                    onClick = {() => this.props.tabClicked(1)}
                    href = "#nav-tab1"
                    style={{textDecoration : 'none'}}
                    >Tab 1</a>

                    <a  
                    className = {"navTab" + (this.props.activeTab === 2 ? ' active' : '')} 
                    onClick = {() => this.props.tabClicked(2)}
                    href = "#nav-tab2"
                    style={{textDecoration : 'none'}}
                    >Tab 2</a>

                    <a  
                    className = {"navTab" + (this.props.activeTab === 3 ? ' active' : '')} 
                    onClick = {() => this.props.tabClicked(3)}
                    href = "#nav-tab3"
                    style={{textDecoration : 'none'}}
                    >Tab 3</a>
                </nav>

                <div className="tabContent">
                    <div 
                    className = { "tabContent-item" + (this.props.activeTab === 1 ? ' show-active' : '')}>
                        <h5>
                            Tab 1  is Clicked
                        </h5>
                    </div>

                    <div 
                    className = { "tabContent-item" + (this.props.activeTab === 2 ? ' show-active' : '')}>
                        <h5>
                            Tab 2  is Clicked
                        </h5>
                    </div>
                    
                    <div 
                    className = { "tabContent-item" + (this.props.activeTab === 3 ? ' show-active' : '')}>
                        <h5>
                            Tab 3  is Clicked
                        </h5>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
//select actionReducer.activeTab roi truyen vo activeTab o day?????
function mapStateToProps(state) {
    return {
        activeTab: state.actionReducer.activeTab,
    }
}
//mapStateToProps: bo loc(filter) cho phep select cac thu trong store mak component can
//Sau do gan chung voi props cua component
//connect()(): Biet Store o dau.
//La lien ket voi Store, mapStateToProps can lien ket nay de toi Store va lay data

const mapDispatchToProps = { tabClicked };
//mapDispatchToProps: la 1 actiocCreator(function...j do) or Object chua cac AC(function...)

//export thoy
const DuplicateTabContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(DuplicateTabComponent);
export default DuplicateTabContainer;


