import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getSongList } from '../../actions/';
import SongDetail from '../songDetail/SongDetail.jsx'

//SongList la 1 container = components giao tiep voi redux = connect {from react-redux}
class songList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isViewingSongsDetail: false,//Xac dinh bay gio show SongDetail hay SongList
            selectedSongId: 0
        }
    }
    //Do sth before render at both clientSide and ServerSide
    componentWillMount() {
        this.props.getSongList();//truoc khi render can lay data(SongList truoc)
    }

    clicked = (songID) => {
        this.setState({
            selectedSongId: songID, //Click vo cai Song nao thi ga'n selectedSongId = songID
            isViewingSongsDetail: true//Chuyen sang che do show detail 1 Song
        });
    }

    hideSongDetail = () => {
        this.setState({ isViewingSongsDetail: false });
        //chay ham nay thi se chuyen ve trang thai show SongList
    }

    render() {

        return (
            <div className="container">
                {(this.state.isViewingSongsDetail) ?
                    <SongDetail
                        sharedSongId={this.state.selectedSongId}
                        onHideSongDetail={this.hideSongDetail} /> :

                    <div className="songList">
                        <div className="songList-item">
                            <a onClick={() => this.clicked(option.id)} style={{'cursor': 'pointer'}}>
                                <p>Den- 2 trieu nam</p>
                            </a>
                        </div>
                        <div className="songList-item">
                            <a onClick={() => this.clicked(option.id)} style={{'cursor': 'pointer'}}>
                                <p>Den- Bai nay Chill phet</p>
                            </a>
                        </div>
                    </div>
                }
            </div>
        )
    }
}



function mapStateToProps(state) {
    return { SongList: state.actionReducer.SongList }
}

const mapDispatchToProps = { getSongList };

//--------------------------------------------------------------------------------------------
const SongList = connect(
    mapStateToProps,
    mapDispatchToProps
)(SongList);

export default SongList;